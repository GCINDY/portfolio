# portfolio-app

#### portfolio App, 3-3-2018

#### By Conquerers

## Description

calculator-app is an application that performs calculations such as addition, subtraction, multiplication and division.

## Setup/Installation Requirements

To pull the application and perform changes, kindly perform...

* On GitHub, navigate to the main page of the repository.
* Under the repository name, click Clone or download.
* In the Clone with HTTPs section, click  to copy the clone URL for the repository.
* Open Terminal.
* Change the current working directory to the location where you want the cloned directory to be made.
* Type git clone, and then paste the URL you copied in Step 2.
* Press Enter. Your local clone will be created.

## Link to live site

https://brendaokumu.github.io/calculator-app/

## Technologies Used

* JavaScript

## Support and contact details

Kindly contact Brenda on brenda.okumu2@gmail.com to make a contribution to the code.
